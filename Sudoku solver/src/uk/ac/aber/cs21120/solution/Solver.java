package uk.ac.aber.cs21120.solution;

import uk.ac.aber.cs21120.interfaces.IGrid;
import uk.ac.aber.cs21120.interfaces.ISolver;

import java.sql.PreparedStatement;

/**
 * a class for solving sudoku puzzles using recursion
 * @author jacob jones
 * @version 1.0(20/11/2020)
 */
public class Solver implements ISolver {
    /**
     * defining a grid object and a integer variable
     */

    IGrid grid;
    int depth;

    /**
     * a constructor for Solver
     * that sets a input grid to a local one
     * @param g an input of a grid
     */
    public Solver(IGrid g) {
         grid =g;
    }

    /**
     * uses a recursive method to solve a sudoku puzzle
     * for loops to go through each position in a grid and to determine
     * the input value
     * @return a boolean which is true if the puzzle is solved
     */
    @Override
    public boolean solve() {
        for (int i =0; i <9 ; i++){
            for (int k =0; k<9; k++) {
                if (grid.get(k,i) ==0 ){
                    for (int j =1; j<10;j++){
                        //System.out.println(depth);
                        grid.set(k,i,j);
                        if(grid.isValid()){
                            boolean result = solve();
                            depth++;
                            if (result){
                                depth--;
                                return true;
                            }
                        }
                    }
                    grid.set(k,i,0);
                    return false;
                }
            }
        }
        return true;
    }
}
