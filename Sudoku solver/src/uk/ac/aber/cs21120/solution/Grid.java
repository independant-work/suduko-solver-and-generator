package uk.ac.aber.cs21120.solution;

import uk.ac.aber.cs21120.interfaces.IGrid;

/**
 * a class called grid which is used in a
 * sudoku puzzle solver and creator
 *
 * @author Jacob Jones
 * @version 1.0(20/11/2020)
 */
public class Grid implements IGrid {
    // a 2d array to store a sudoko puzzle
    private int[][] grid = new int[9][9];

    //a constructor which fills the array with 0 or what is considered empty spaces
    public Grid() {
        for (int i = 0; i < 9; i++) {
            for (int k = 0; k < 9; k++) {
                grid[i][k] = 0;
            }
        }
    }

    /**
     *  a method that gets a value from a position in an 2d array
     * @param x column number
     * @param y row number
     * @return a value stored at position xy
     * @throws BadCellException
     */
    @Override
    public int get(int x, int y) throws BadCellException {
        if (((x>9)||(x<0)) ||((y>9)||(y<0))){
            throw new BadCellException(x, y);
        }
        int valstor = grid[x][y];
        if ((grid[x][y] <0)||(grid[x][y] >9) ){
            valstor =0;
        }
        return valstor;
    }

    /**
     * sets a grid position to a value
     * @param x column number
     * @param y row number
     * @param val digit from 1-9, or 0 for an empty cell
     * @throws BadCellException
     * @throws BadDigitException
     */
    @Override
    public void set(int x, int y, int val) throws BadCellException, BadDigitException {
        if (val<0 || val >9){
            throw new BadDigitException(val);
        }
        if ((x>8)||(x<0)||(y>8)||(y<0)){
            throw new BadCellException(x, y);
        }

        grid[x][y] = val;
    }

    /**
     * checks if a position in the grid is equal to 0
     * @param i
     * @param j
     * @return a boolean
     */
    private boolean notZero(int i, int j){
        if (grid[i][j] != 0){
            return true;
        }
        return false;
    }

    /**
     * prints out the grid to the console in a grid like pattern
     *
     */
    public void printGrid (){
        for (int i = 0; i < 9; i++) {
            for (int k = 0; k < 9; k++) {
                System.out.print(grid[i][k]);
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * prints the number of gaps in a sudoku puzzle
     */
    public void printGaps(){
        int numberOfGaps =0;
        for (int i = 0; i < 9; i++) {
            for (int k = 0; k < 9; k++) {
                if (grid[i][k] == 0){
                    numberOfGaps++;
                }
            }
        }
        System.out.println(numberOfGaps);
    }

    /**
     * checks if a grid containing a sudoku puzzle
     * complies with the given rules
     * @return a boolean if the puzzle is compliant
     */
    @Override
    public boolean isValid() {
        boolean rowsvalid = true;
        boolean columnvalid =true;
        boolean subgridvalid =true;

        int incrementx =0;
        int incrementy =0;

        for (int j=0;j<9;j++) {
            for (int i = 0; i < 9; i++) {
                for (int k = 0; k < 9; k++) {
                    if ((grid[i][j] == grid[k][j]) && (i != k) && (notZero(i,j))) {
                        rowsvalid = false;
                    }
                    if ((grid[j][i] ==grid[j][k]) &&(i != k) && (notZero(j,i)) ){
                        columnvalid = false;
                    }
                }
            }
            int[] tempstor = new int[9];
            int numstor = 0;
            for (int i = incrementx; i < incrementx+3; i++) {
                for (int k = incrementy; k < incrementy+3; k++) {

                    tempstor[numstor] = grid[i][k];
                    numstor++;

                }
            }
            for (int i = 0; i < 9; i++) {
                for (int k = 0; k < 9; k++){
                    if (((tempstor[i] == tempstor[k]) && (i != k)) && (tempstor[i] !=0)) {
                        subgridvalid = false;
                    }
                }
            }

            if (subgridvalid){
                if (incrementx ==6){
                    incrementx =0;
                    incrementy = incrementy+3;
                }
                else {
                    incrementx = incrementx+3;
                }
            }
        }


        //printGrid();
        return rowsvalid && columnvalid && subgridvalid;
    }

}
