package uk.ac.aber.cs21120.solution;

import uk.ac.aber.cs21120.tests.Examples;

/**
 * the main class for the sudoku solver program
 * @author jacob jones
 * @version 1.0(20/11/2020)
 */
public class Main {
    public static void main(String[] args) {

        /*
        this is the puzzle generator class and how it can be used

        Generator gen = new Generator();
        Grid grids = new Grid();
        grids = gen.makePuzzle();
        Solver sole = new Solver(grids);
        System.out.println(sole.solve());
        */

        //this is the main class meant for task 3
        for (int i =0; i<401;i++) {
            Solver solveMe = new Solver(Examples.getExample(i));
            long start = System.currentTimeMillis();

            boolean solved =solveMe.solve();

            float end = System.currentTimeMillis()-start;
            float sec = end /100;


            if (solved) {
                System.out.println("the puzzle was solved ");
            }else {
                System.out.println("the puzzle cant be solved ");
            }

            System.out.print("puzzle number is ");
            System.out.println(i);
            System.out.print("gap number is ");
            System.out.println(Examples.getGapCount(i));
            System.out.print("time is ");
            System.out.print(sec);
            System.out.println(" seconds");
            System.out.println();

        }
    }
}
