package uk.ac.aber.cs21120.solution;
import uk.ac.aber.cs21120.interfaces.IGrid;
import java.util.Random;

/**
 * this is a class that is used to generate new sudoku
 * puzzles using the methods from Grid and Solver
 * @author jacob jones
 * @version (20/11/2020)
 */
public class Generator {
    /**
     * makes new objects of grid and solver class
     */
    private Grid newpuzzle = new Grid();
    private Solver sol = new Solver(newpuzzle);

    /**
     * makes new puzzles using random number generation
     * it then checks if the puzzles are valid and solvable
     * @return a valid solvable puzzle stored in a newpuzzle
     */
    Grid makePuzzle (){
        do {
            for (int i = 0; i < 9; i++) {
                for (int k = 0; k < 9; k++) {
                    Random random = new Random();
                    int store = random.nextInt(9);
                    newpuzzle.set(i, k, store);
                    if (!newpuzzle.isValid()) {
                        newpuzzle.set(i, k, 0);
                    }
                }
            }

            newpuzzle.printGrid();
        }while (!sol.solve());

        return newpuzzle;
    }

}
